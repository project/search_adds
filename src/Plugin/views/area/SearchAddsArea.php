<?php

namespace Drupal\search_adds\Plugin\views\area;

use Drupal\views\Plugin\views\area\AreaPluginBase;

/**
 * Provides an area for the triggered text.
 *
 * @ingroup views_area_handlers
 *
 * @ViewsArea("search_adds_area")
 */
class SearchAddsArea extends AreaPluginBase {

  /**
   * Render the area.  This is the minimal required definition.
   *
   * @param bool $empty
   *   (optional) Indicator if view result is empty or not. Defaults to FALSE.
   *
   * @return array
   *   In any case we need a valid Drupal render array to return.
   */
  public function render($empty = FALSE) {
    // Retrieve our trigger list.
    $config = \Drupal::config('search_adds.settings');
    // Collect the search text.
    $searchFilter = $config->get('searchfilter');
    $query = $this->view->exposed_data[$searchFilter] ?? '';
    $limit = $config->get('counted');
    // All letters to lowercase and remove punctuation.
    $fuzzy_search = preg_replace('/[^a-z0-9]/', '', strtolower($query));

    $message = '';

    for ($i = 1; $i <= $limit; $i++) {
      $trigger = $config->get('trigger_' . $i);
      $fuzzy_trigger = preg_replace('/[^a-zA-Z0-9]/', '', strtolower($trigger));
      $suggestion = $config->get('response_' . $i);

      $useRegex =  $config->get('regex_' . $i);

      if ($useRegex) {
        // Use regex to match the trigger.
        $match = preg_match($trigger, $query);
        if($match){
          $message .= '<div class="well"><span class="search-adds-text">' . $suggestion . '</span></div>';
        }
      }else{
        if (strpos($query, $trigger) !== FALSE) {
          $message .= '<div class="well"><span class="search-adds-text">' . $suggestion . '</span></div>';
          // Exact match.
        } else if (strpos($fuzzy_search, $fuzzy_trigger) !== FALSE) {
          $message .= '<div class="well"><span class="search-adds-text">' . $suggestion . '</span></div>';
          // Meh, close enough.
        }
      }


    }
    if(!empty($message)){
      return [
        '#markup' => $message,
      ];
    }

    return [];
  }

}
